#!/usr/bin/env bash

set -e
set -o pipefail

file="$1"
stage="$2"

cp $file $CUSTOM_ENV_CI_PROJECT_DIR/$(basename $file)
file=${CUSTOM_ENV_CI_PROJECT_DIR}/$(basename $file)

NAME="$CUSTOM_ENV_CI_COMMIT_SHORT_SHA-$CUSTOM_ENV_CI_JOB_ID"
JOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

srun --overlap --jobid=$JOBID --chdir=${CUSTOM_ENV_CI_PROJECT_DIR} podman exec $CONTAINER_NAME mkdir -p "$(dirname $file)"
srun --overlap --jobid=$JOBID --chdir=${CUSTOM_ENV_CI_PROJECT_DIR} podman cp "$file" $CONTAINER_NAME:"$file"
if ! srun --overlap --jobid=$JOBID podman exec $CONTAINER_NAME "$file" ; then
    exit $BUILD_FAILURE_EXIT_CODE
fi

exit 0
