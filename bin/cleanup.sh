#!/bin/bash

NAME="$CUSTOM_ENV_CI_COMMIT_SHORT_SHA-$CUSTOM_ENV_CI_JOB_ID"
JOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

srun --overlap --jobid=$JOBID bash -c "podman attach $CONTAINER_NAME <<< 'exit' ; podman rm -f $CONTAINER_NAME ; true"
scancel "$JOBID"

exit 0
