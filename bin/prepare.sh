#!/bin/bash

set -e
set -o pipefail

[[ ! -v CUSTOM_ENV_CSCS_CI_MW_URL ]] && CUSTOM_ENV_CSCS_CI_MW_URL="https://cicd-ext-mw.cscs.ch/ci"


CURRENT_DIR=$(dirname $(realpath "${BASH_SOURCE[0]:-$0}"))

NAME="$CUSTOM_ENV_CI_COMMIT_SHORT_SHA-$CUSTOM_ENV_CI_JOB_ID"
OLDJOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

if [[ -z "$OLDJOBID" ]]; then
    echo -e "Allocating a job with name ${b}$NAME${x}."

    NUM_NODES=1

    start_alloc=$(date +%s)
    ( set -x ; salloc \
        --no-shell \
        --time="$(($CUSTOM_ENV_CI_JOB_TIMEOUT / 60 - 5))" \
        --job-name="$NAME" \
        --nodes=${NUM_NODES} \
    )
    if [ $? -ne 0 ]; then
        echo "Could not allocate resources"
        exit "$BUILD_FAILURE_EXIT_CODE"
    fi
    end_alloc=$(date +%s)
    let waiting_alloc=($end_alloc - $start_alloc)
    echo "Waited $waiting_alloc seconds for allocation"

    # start container on compute node
    JOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

    # send job meta data to middleware
    if [[ -z "$CLUSTER_NAME" && -f /etc/xthostname ]] ; then
        CLUSTER_NAME=$(cat /etc/xthostname)
    fi
    JOB_META_DATA='{"mode":"container-builder","allocation_wait_time":'$waiting_alloc',"slurm_job_id":"'$JOBID'","machine":"'$CLUSTER_NAME'","num_nodes":'$NUM_NODES'}'
    curl -s --retry 5 --retry-connrefused -H "Content-Type: application/json" --data-raw "${JOB_META_DATA}" "${CUSTOM_ENV_CSCS_CI_MW_URL}/redis/job?token=${CUSTOM_ENV_CI_JOB_TOKEN}&job_id=${CUSTOM_ENV_CI_JOB_ID}"
else
    echo -e "Using existing allocation ${b}${OLDJOBID}${x} with name ${b}${NAME}${x}"
    JOBID="${OLDJOBID}"
fi

mkdir -p ${CUSTOM_ENV_CI_PROJECT_DIR}
# cleanup previous potential problems
srun --jobid=$JOBID --chdir=${CUSTOM_ENV_CI_PROJECT_DIR} podman system migrate
srun --jobid=$JOBID --chdir=${CUSTOM_ENV_CI_PROJECT_DIR} podman system reset -f

IMG="jfrog.svc.cscs.ch/docker-ci-ext/6222709432565718/public/cicd-ext-k8-container-image-builder:podman-4.9.0"
if [[ $CUSTOM_ENV_CI_JOB_IMAGE == *-devel ]] ; then
    IMG="docker.io/finkandreas/cicd-ext-k8s-container-image-builder:podman-4.9.0-devel"
fi

# start a container and move to background
bash -c "srun --jobid=$JOBID --chdir=${CUSTOM_ENV_CI_PROJECT_DIR} podman run --pull=always -v $(which gitlab-runner):/usr/bin/gitlab-runner:ro --device=/dev/fuse --cap-add SYS_ADMIN --cap-add NET_RAW --cap-add MKNOD --cap-add AUDIT_WRITE --rm -it --name $CONTAINER_NAME $IMG &" >${CUSTOM_ENV_CI_PROJECT_DIR}/container_startup.log 2>&1 &

while ! srun --overlap --jobid=$JOBID --chdir=${CUSTOM_ENV_CI_PROJECT_DIR} podman ps -a 2>/dev/null | grep $CONTAINER_NAME ; do
    sleep 1
    if ! srun --overlap --jobid=$JOBID pstree -pT -u $(id -un) | grep -q podman ; then
        echo "Starting podman container failed. Log file content:"
        cat ${CUSTOM_ENV_CI_PROJECT_DIR}/container_startup.log
        exit ${SYSTEM_FAILURE_EXIT_CODE:-1}
    fi
done

exit 0
